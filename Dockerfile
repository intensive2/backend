FROM maven:3.5-jdk-11 as build
WORKDIR /app
COPY . .
RUN mvn clean install -Dmaven.test.skip=true

FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=build /app/target/*.jar /app/studentsystem.jar
EXPOSE 8080
ENTRYPOINT [ "java" ]
CMD [ "-jar", "studentsystem.jar" ]